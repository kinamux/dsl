/*
 *  AnLRUCache.js
 *      Object with functions for a simple LRU Cache
 *
 *  Dependencies
 *      Hash Dictionary that is built into JavaScript
 *
 *  Big ToDo:
 *      Handle multi-threaded access if required
 *      Optimize time for eviction (and hence new insertion)
 *      Optimize storage costs for hashes in JavaScript
 */

 (function() {

'use strict';

 	/* 
 	 * LRUItem - Object that keeps track of key, value, and tick information
 	 */
 	var LRUItem = function( key, value, tick)
 	{
 		this.key = key;
 		this.value = value;
 		this.lruTick = tick;
 		this.stale = false; // indicate an item is ready for removal
 	}

 	// Just update this item to new value with specified tick
 	LRUItem.prototype.update = function( value, tick)
 	{
 		this.value = value;
 		this.lruTick = tick;
 		this.stale = false;
 	}

 	// Just update the tick for this item
 	LRUItem.prototype.touch = function( tick)
 	{
 		this.lruTick = tick;
 		this.stale = false;
 	}


 	LRUItem.prototype.Print = function( index) 
 	{
 		console.log( "\t[%d] tick=%d; stale=%s; key=%s; value=%s;",
 			index, this.lruTick, this.stale, this.key, this.value
 			);
 	}

 	var MAX_LRU_CAPACITY = 10000; // a constant limit to keep memory pressure limited
	/*
	 *  AnLRUCache - create the object for managing the LRU Cache
	 *
	 *  @param{Object} [key] - key for the item to be added
	 *  @param{Object} [value] - value for the item to be added
	 *
	 *  @returns None
	 */
	var AnLRUCache = function( lruOptions, fVerbose)
	{
		// Validate arguments
		if (lruOptions) {
			if (lruOptions.capacity <= 0) {
				throw { name: "Invalid Arugment", message: "Capacity cannot be negative"};
			}

			if (lruOptions.capacity > MAX_LRU_CAPACITY) {
				throw { name: "Invalid Arugment", message: "Capacity requested exceeds internal limit of " + MAX_LRU_CAPACITY};
			}
		}

		if (typeof fVerbose !== 'boolean') {

			throw { name: "Invalid Arugment", message: "fVerbose specified is not of boolean type"};
		}

		var self = this;
		this.fVerbose = fVerbose;
		this.capacity = lruOptions ? (lruOptions.capacity || 10) : 10;
		this.allOptions = lruOptions;

		this.cache = {}; // Initialize to an empty hash table for holding index of cached items
		this.itemsInCache = []; // an array of LRU Items in use
		this.numItems = 0;
		this.masterTick = 0; // master clock tick for use in this LRU object

	}

	AnLRUCache.prototype.NumItems = function() 
	{
		return this.numItems;
	}

	AnLRUCache.prototype.Capacity = function()
	{
		return this.capacity;
	}

	AnLRUCache.prototype.isFull = function()
	{
		return (this.numItems == this.capacity);
	}

	AnLRUCache.prototype.isNotFull = function()
	{
		return (this.numItems < this.capacity);
	}

	AnLRUCache.prototype.Print = function()
	{
		console.log( "\n\t---------- LRU Cache  ------------- ");
		console.log( "\tAnLRUCache:  numItems=%d;  capacity=%d",
			this.numItems, this.capacity);

		// ToDo: Print the cache itself
		console.log( "\t---------- Items in Hash ------------- ");
		var keys = Object.keys( this.cache);
		keys.forEach( function(d, i) {
			console.log("\t[%d] key=%s -> %d. value=%s", 
				i, d, this.cache[d], this.itemsInCache[this.cache[d]].value);
		}, this)

		console.log( "\t---------- Cached Items In Use ------------- ");
		this.itemsInCache.forEach( function( item, i) {
			item.Print( i);
		})
	}

	AnLRUCache.prototype.addToLruList = function( newItem, indexToAddAt)
	{
		if (this.fVerbose) {
			console.log( "addToLruList(key=%s, tick=%d) at [%d] with LRU List at (numItems=%d, masterTick=%d)",
				newItem.key, newItem.lruTick, indexToAddAt, this.numItems, this.masterTick);
		}

		if ( this.isFull()) {
			throw { name: "Invalid Call", message: "addToLruList called when list is full"};
		}

		// ToDo: optimize to avoid the linear scans for eviction
		this.itemsInCache[indexToAddAt] = newItem;
	}

	AnLRUCache.prototype.removeItem = function( indexOfItem)
	{

		var itemToEvict = this.itemsInCache[indexOfItem];
		if (this.fVerbose) {
			console.log( "  removing item at [%d] (key=%s, tick=%d) from LRU list",
				indexOfItem, itemToEvict.key, itemToEvict.lruTick);
		}

		// Mark it stale to indicate that this item is ready for eviction
		itemToEvict.stale = true;
		delete this.cache[ itemToEvict.key];

		// ToDo: optimize away this unwanted copy ... that means we cannot decrement numItems
		this.itemsInCache[indexOfItem] = this.itemsInCache[this.numItems-1];

		this.numItems--;

		return;
	}

	AnLRUCache.prototype.evictAnItem = function( )
	{
		if (this.fVerbose) {
			console.log( "evictAnItem() with LRU List at (numItems=%d, masterTick=%d)",
				this.numItems, this.masterTick);
		}

		if ( this.isNotFull()) {
			throw { name: "Invalid Call", message: "evictAnItem called when list is not full"};
		}

		// ToDo: optimize to avoid the linear scans for eviction

		// Scan the list and evict the oldest item from the list
		var indexOfOldItem = 0;
		var minTick = this.itemsInCache[0].lruTick;
		for( var i = 1; i < this.numItems; i++) {
			if ( this.itemsInCache[i].lruTick < minTick) {
				indexOfOldItem = i;
				minTick = this.itemsInCache[i].lruTick;
			}
		}

		// remove this item from the list
		this.removeItem( indexOfOldItem);

		return indexOfOldItem;
	}

	function isValidArguments( key, value)
	{
		return ((typeof key === 'string') || (typeof key === 'number'));
	}

	/*
	 *  addItem() - Attempt an O(1) operation to add a new item
	 *
	 *  @param{Object} [key] - key for the item to be added
	 *  @param{Object} [value] - value for the item to be added
	 *
	 *  @returns None
	 */
	AnLRUCache.prototype.addItem = function( key, value)
	{

		if (this.fVerbose) {
			console.log( " AnLRUCache:addItem( key=%s, value=%s)", key, value);
		}

		// 1. Check if the input is valid
		if (!key) {
			// ToDo: Find the suitable JavaScript null argument exception to throw
			throw { name: "Null Argument", message: "No key value is specified"};
		}

		if (!isValidArguments( key, value)) {

			throw { name: "Invalid Argument", message: "addItem() supplied arguments are not of suitable type"};
		}


		// 2. Is this item already in the cache. If so, update the value instead
		var itemIndex = this.cache[key];
		var itemNow = (itemIndex == undefined) ? null : this.itemsInCache[itemIndex];
		if (itemNow) {
			// just update the value
			itemNow.update( value, this.masterTick++);
			return;
		}

		// 3. Check if there is room for this item, if not we have to evict someone
		var indexToAddAt = (this.isFull()) ? this.evictAnItem() : this.numItems;

		// 4. Create a new item and add to the cache
		var newItem = new LRUItem( key, value, this.masterTick++);

		this.cache[key] = indexToAddAt;
		this.addToLruList( newItem, indexToAddAt);
		this.numItems++;
	}

	/*
	 *  getItem() - Get an item from the cache
	 * 		if present, then the item's LRU tick is refreshed.
	 *
	 *  @param{Object} [key] - key for the item to be found
	 *
	 *  @returns {Object} - returns the value object if found. 
	 *		else it returns null
	 */
	AnLRUCache.prototype.getItem = function( key)
	{

		if (this.fVerbose) {
			console.log( " AnLRUCache:getItem( key=%s)", key);
		}

		// 1. Check if the input is valid
		if (!key) {
			// ToDo: Find the suitable JavaScript null argument exception to throw
			throw { name: "Null Argument", message: "No key value is specified"};
		}

		if (!isValidArguments( key, value)) {

			throw { name: "Invalid Argument", message: "getItem() supplied arguments are not of suitable type"};
		}

		// 2. Is this item already in the cache. If so, update the value instead
		var itemIndex = this.cache[key];
		var itemNow = (itemIndex == undefined) ? null : this.itemsInCache[itemIndex];
		var value = null;
		if (itemNow && (itemNow.stale == false)) {
			// just update the value
			itemNow.touch( this.masterTick++);
			value = itemNow.value;
		} 

		return value;
	}

	/*
	 *  deleteItem() - Delete an item from the cache
	 * 		if present, then the item is marked stale and removed.
	 *
	 *  @param{Object} [key] - key for the item to be deleted
	 *
	 *  @returns {Object} - returns the value object if found and deleted
	 *		else it returns null
	 */
	AnLRUCache.prototype.deleteItem = function( key)
	{

		if (this.fVerbose) {
			console.log( " AnLRUCache:deleteItem( key=%s)", key);
		}

		// 1. Check if the input is valid
		if (!key) {
			// ToDo: Find the suitable JavaScript null argument exception to throw
			throw { name: "Null Argument", message: "No key value is specified"};
		}

		if (!isValidArguments( key, value)) {

			throw { name: "Invalid Argument", message: "deleteItem() supplied arguments are not of suitable type"};
		}

		// 2. Is this item already in the cache. If so, update the value instead
		var itemIndex = this.cache[key];
		var itemNow = (itemIndex == undefined) ? null : this.itemsInCache[itemIndex];
		var value = null;
		if (itemNow && !itemNow.stale) {
			value = itemNow.value;
			this.removeItem( itemIndex);
		} 

		return value;
	}

	// ----------------------------------------------------------------
	// Module Interface for external use
    if ((typeof module) === 'undefined') {
        window.AnLRUCache =  AnLRUCache;
    } else if ((typeof module === 'object') && module.exports) {
		module.exports = AnLRUCache;
	}
})();
