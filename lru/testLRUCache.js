/*
 *  testLRUCache.js
 *      Test the LRU Cache
 *
 *  Dependencies
 *      AnLRUCache
 *
 *   Goal of the tests
 *		1. Nothing should crash
 *		2. Good errors are found
 *		3. Solution is correct
 *		4. Solution is optimal
 */

// ------------------------------------------------------------------
// Dependencies
var LRUCache = require('./AnLRUCache');

// ------------------------------------------------------------------
//  T E S T    F U N C T I O N S

// 1. Create and Print LRU Cache
function test1( fVerbose) 
{
	console.log( "\n\n----------------------------------");
	console.log( " Test1: Create and Print LRU Cache");
	var lruCache1 = new LRUCache( null, fVerbose);

	lruCache1.Print();
}

// 2. Create, add n items LRU Cache
function testAddItems( fVerbose, maxItems, numItems) 
{
	console.log( "\n\n----------------------------------");
	console.log( " Test AddItems: Create LRUCache (size=%d), add %d items and Print LRU Cache", 
		maxItems, numItems);
	var lruCache1 = new LRUCache( { capacity: maxItems}, fVerbose);

	for(var i=1; i<= numItems; i++) {
		var item = { key: i, value: i*i};

		lruCache1.addItem( item.key, item.value);
	}

	lruCache1.Print();
}

// 3. Create, add n items, get m items, and then add p items
function testAddAndGet( fVerbose, maxItems, numItemsAdd1, numItemsGet, numItemsAdd2) 
{
	console.log( "----------------------------------");
	console.log( " Test Add&Get: Create, add n items, get m items, and then add p items");
	var lruCache1 = new LRUCache( { capacity: maxItems}, fVerbose);

	console.log( "\n  1. Add %d items first ", numItemsAdd1);
	for(var i=1; i<= numItemsAdd1; i++) {
		var item = { key: i, value: i*i};

		lruCache1.addItem( item.key, item.value);
	}

	lruCache1.Print();

	console.log( "\n  2. Add some %d random touches", numItemsGet);
	for( var j = 0; j < numItemsGet; j++) {
		var keyToGet = Math.floor( Math.random(0, 1) * (maxItems + 2)) + 1;
		var item = lruCache1.getItem( keyToGet);

		if (item == null) {
			console.log( "        Found no item for keyToGet:%d", keyToGet);
		}  else {
			console.log( "      KeyToGet[%d] gives value %s", keyToGet, item);
		}
	}

	lruCache1.Print();

	console.log( "\n  3. Add %d items next ", numItemsAdd2);
	for(var i=numItemsAdd1 +1; i<= (numItemsAdd1 + numItemsAdd2); i++) {
		var item = { key: i, value: i*i};

		lruCache1.addItem( item.key, item.value);
	}

	lruCache1.Print();
	return;
}

// 4. Add n items, get, delete, and check all is well
function testAddGetDelete( fVerbose, maxItems, numItemsAdd1, numItemsGet, numItemsDelete) 
{
	console.log( "----------------------------------");
	console.log( " Test Add&Get&Delete:  Add n items, get, delete, and check all is well");
	var lruCache1 = new LRUCache( { capacity: maxItems}, fVerbose);

	console.log( "\n  1. Add %d items first ", numItemsAdd1);
	for(var i=1; i<= numItemsAdd1; i++) {
		var item = { key: i, value: i*i};

		lruCache1.addItem( item.key, item.value);
	}

	lruCache1.Print();

	console.log( "\n  2. Add some %d random touches", numItemsGet);
	for( var j = 0; j < numItemsGet; j++) {
		var keyToGet = Math.floor( Math.random(0, 1) * (maxItems + 2)) + 1;
		var item = lruCache1.getItem( keyToGet);

		if (item == null) {
			console.log( "        Found no item for keyToGet:%d", keyToGet);
		}  else {
			console.log( "      KeyToGet[%d] gives value %s", keyToGet, item);
		}
	}

	lruCache1.Print();

	console.log( "\n  3. Delete %d items next ", numItemsDelete);
	for(var i=1; i<= numItemsDelete; i++) {
		var keyToGet = Math.floor( Math.random(0, 1) * (maxItems + 2)) + 1;

		var item = lruCache1.deleteItem( keyToGet);

		if (item == null) {
			console.log( "        Found no item for keyToGet:%d", keyToGet);
		}  else {
			console.log( "      KeyToGet[%d] deleted item for value %s", keyToGet, item);
		}
	}

	lruCache1.Print();
	return;
}

// ------------------------------------------------------------------
//  TEST THESE ALL
var fVerbose = false;
test1(fVerbose);

// testAddItems( fVerbose, 3, 3);
testAddItems( fVerbose, 3, 6);
// testAddItems( fVerbose, 3, 27);
// testAddItems( fVerbose, -33, 27); // Should Fail for negative capacity

testAddAndGet( fVerbose, 3, 3, 10, 2);

// testAddGetDelete( fVerbose, 3, 3, 10, 2);
testAddGetDelete( fVerbose, 3, 3, 10, 7);

